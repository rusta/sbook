/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Button } from "react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

export default class App extends Component {
  state = {
    buttonPressed: false
  };
  pressButton = () => {
    this.setState({
      buttonPressed: !this.state.buttonPressed
    });
  };
  render() {
    const { buttonPressed } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome} accessibilityLabel="text1">
          Welcome to React Native - RUSS DEBUG 2!
        </Text>
        <Button
          onPress={this.pressButton}
          title="Test Button"
          accessibilityLabel="testButton"
        />
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
        {buttonPressed && (
          <Text style={styles.welcome} accessibilityLabel="text2">
            button pressed
          </Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
