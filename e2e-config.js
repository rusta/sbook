const devices = {
  "ios-emulator": {
    platformName: "iOS",
    platformVersion: "11.4",
    deviceName: "iPhone 6",
    app:
      "/Users/russw/Dev/storybook-debug/sbook/ios/build/Build/Products/Debug-iphonesimulator/sbook.app"
  },
  "android-emulator": {
    platformName: "Android",
    deviceName: "Android Emulator",
    app: "./android/app/build/outputs/apk/app-release.apk"
  }
};

if (!process.env.E2E_DEVICE) {
  throw new Error("E2E_DEVICE environment variable is not defined");
}

if (!devices[process.env.E2E_DEVICE]) {
  throw new Error(
    `No e2e device configuration found in package.json for E2E_DEVICE environment ${
      process.env.E2E_DEVICE
    }`
  );
}

export default devices[process.env.E2E_DEVICE];
