import wd from "wd";
import config from "../e2e-config";

const port = 4723;
const driver = wd.promiseChainRemote("localhost", port);
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

describe("Simple Appium Example", () => {
  beforeAll(async () => await driver.init(config));
  afterAll(async () => await driver.quit());
  it("login screen renders", async () => {
    console.log("CAN YOU SEE ME?");
    console.log(1);
    // expect(await driver.hasElementByAccessibilityId("text1")).toBe(true);

    // wait for text to exist
    expect(
      await driver.waitForElementByXPath(
        '//XCUIElementTypeStaticText[@name="text1"]',
        15000,
        2000
      )
    ).toBeDefined();

    console.log(2);

    // check that button exists
    const testButtonExists = await driver.hasElementByXPath(
      '//XCUIElementTypeButton[@name="testButton"]'
    );
    console.log({ testButtonExists });
    expect(testButtonExists).toBeTruthy();

    console.log(3);

    // check that text2 doesn't exist
    let text2Exists = await driver.hasElementByXPath(
      '//XCUIElementTypeStaticText[@name="text2"]'
    );
    expect(text2Exists).toBeFalsy();

    console.log(4);

    // click the button
    // await driver
    //   .waitForElementByXPath(
    //     '//XCUIElementTypeButton[@name="testButton"]',
    //     15000,
    //     2000
    //   )
    //   .click();

    const button = await driver.waitForElementByXPath(
      '//XCUIElementTypeButton[@name="testButton"]',
      15000,
      2000
    );
    button.click();

    // console.log({
    //   testButton
    // });

    // testButton.tap();

    console.log(5);

    await driver.waitForElementByXPath(
      '//XCUIElementTypeStaticText[@name="text2"]',
      15000,
      2000
    );

    // // check that text2 DOES exist
    text2Exists = await driver.hasElementByXPath(
      '//XCUIElementTypeStaticText[@name="text2"]'
    );
    expect(text2Exists).toBeTruthy();

    // console.log(6);

    // // click the button again
    // testButton.click();

    // console.log(7);

    // text2Exists = await driver.hasElementByXPath(
    //   '//XCUIElementTypeStaticText[@name="text2"]'
    // );
    // expect(text2Exists).toBeFalsy();

    console.log(8);
  });
});
